# Click And Loot

The game you don't want to play - *SimplonGangCompany*

## Dépendances

- PHP >= 7.2
- Composer à jour
- NPM / Node

## Installation

```
$> git submodule update --init --recursive
$> cd back/ClickAndLootApi  composer install
```

## Lancement

```
$> docker-compose up
```

## MySql et Adminer

Adminer est accessible sur le port 20003.

Identifiants du serveur MySQL :
- Serveur : click-and-loot-mysql
- Utilisateur / Mot de passe : root / root
- Base de Donnée : Click_And_Loot_Api

## Configuration de l'Api

Pour que l'application fonctionne :
    - On s'assure de configurer le .env, en mettant exactement cette ligne "DATABASE_URL=mysql://root:root@mysql/Click_And_Loot_Api" à la place de la phrase par défaut
    - De poser le squelette de la Db sur notre nouvelle Db via Adminer, on se rend sur Adminer dans notre Db et on importe le fichier SQL joint "DbC&L.sql"
    - Si modifs Db faire les 3 commandes pour exporter la nouvelle Db :
	IL FAUT QUE DOCKER-COMPOSE SOIT ALLUMÉ
	$> docker-compose exec php-fpm bash
	$> cd ClickAndLootApi
	$> php bin/console make:migration
	$> php bin/console doctrine:migrations:migrate (Valider "yes")
	$> exit
    - Votre Db est maintenant à jour;
	


# Pour voir l'angular fonctionner il faut faire une simple sauvegarde dans un fichier .ts, enlever un point virgule puis le remettre et sauvegarder, ça devrait suffire
