-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 18, 2018 at 11:49 AM
-- Server version: 5.7.22-0ubuntu0.17.10.1
-- PHP Version: 7.1.17-0ubuntu0.17.10.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ClickAndLootAPI`
--

-- --------------------------------------------------------

--
-- Table structure for table `cell`
--

CREATE TABLE `cell` (
  `id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  `x_coord` int(11) DEFAULT NULL,
  `y_coord` int(11) NOT NULL,
  `is_discovered` tinyint(1) NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Table structure for table `environnement`
--

CREATE TABLE `environnement` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `environnement`
--

INSERT INTO `environnement` (`id`, `name`, `background`) VALUES
(1, 'Ruin', '../assets/ruin.png'),
(2, 'Depth', '../assets/depth.png'),
(3, 'Field', '../assets/field.png'),
(4, 'Jail', '../assets/jail.png'),
(5, 'Jungle', '../assets/jungle.png'),
(6, 'Void', '../assets/void.png'),
(7, 'Cursed city', '../assets/city.png'),
(8, 'Grave', '../assets/grave.png'),
(9, 'Desert', '../assets/desert.png'),
(10, 'Castle', '../assets/castle.png');

-- --------------------------------------------------------

--
-- Table structure for table `hero`
--

CREATE TABLE `hero` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pseudo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hero_class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_x` int(11) DEFAULT NULL,
  `current_y` int(11) DEFAULT NULL,
  `gold` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `hero_items`
--

CREATE TABLE `hero_items` (
  `id` int(11) NOT NULL,
  `hero_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `id_decoder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `id_decoder` int(11) NOT NULL,
  `complete_item` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suffixe` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rarity_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rarity_color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `str` int(11) NOT NULL,
  `sta` int(11) NOT NULL,
  `agi` int(11) NOT NULL,
  `wis` int(11) NOT NULL,
  `intel` int(11) NOT NULL,
  `luc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `item_type`
--

CREATE TABLE `item_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_type`
--

INSERT INTO `item_type` (`id`, `name`) VALUES
(1, 'weapon_left'),
(2, 'weapon_right'),
(3, 'Ring'),
(4, 'Relic'),
(5, 'Amulet'),
(6, 'Belt'),
(7, 'Helmet'),
(8, 'Torso'),
(9, 'Boots'),
(10, 'Legs'),
(11, 'Gloves'),
(12, 'Arm Band'),
(13, 'Shoulders');

-- --------------------------------------------------------

--
-- Table structure for table `left_handed_weapon`
--

CREATE TABLE `left_handed_weapon` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `left_handed_weapon`
--

INSERT INTO `left_handed_weapon` (`id`, `name`) VALUES
(1, 'Axe'),
(2, 'Dagger'),
(3, 'Mace'),
(4, 'Spear'),
(5, 'Sword'),
(6, 'Staff'),
(7, 'Scythe'),
(8, 'Daibo'),
(9, 'Fist Weapons'),
(10, 'Bow'),
(11, 'Crossbow'),
(12, 'Wand');

-- --------------------------------------------------------

--
-- Table structure for table `map`
--

CREATE TABLE `map` (
  `id` int(11) NOT NULL,
  `hero_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_done` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Table structure for table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20180605083513'),
('20180607114900'),
('20180607122551'),
('20180611113543');

-- --------------------------------------------------------

--
-- Table structure for table `right_handed_weapon`
--

CREATE TABLE `right_handed_weapon` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `right_handed_weapon`
--

INSERT INTO `right_handed_weapon` (`id`, `name`) VALUES
(1, 'Shield'),
(2, 'Mojo'),
(3, 'Orb'),
(4, 'Quiver');

-- --------------------------------------------------------

--
-- Table structure for table `suffixe_item`
--

CREATE TABLE `suffixe_item` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suffixe_item`
--

INSERT INTO `suffixe_item` (`id`, `name`) VALUES
(1, 'Of Sofled'),
(2, 'Of Thrythsha'),
(3, 'Of Janath'),
(4, 'Of Dracmonra'),
(5, 'Of Thanvaror'),
(6, 'Of Gakhor'),
(7, 'Of Bronmon'),
(8, 'Of Diatian'),
(9, 'Of Kraynrhae'),
(10, 'Of Dringli'),
(11, 'Of Fangilmai'),
(12, 'Of Rialoraeg'),
(13, 'Of Wingron'),
(14, 'Of Taalta'),
(15, 'Of Jean-Michel Apathie'),
(16, 'Of Jow la douille'),
(17, 'Of Pissocu'),
(18, 'Of Antéchrist');

-- --------------------------------------------------------

--
-- Table structure for table `suffixe_map`
--

CREATE TABLE `suffixe_map` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suffixe_map`
--

INSERT INTO `suffixe_map` (`id`, `name`) VALUES
(1, 'of the old Thankodon'),
(2, 'of the fallen king Mondpodu'),
(3, 'of the ancient demon Flasroloch'),
(4, 'of the dark lord Carasba'),
(5, 'of the half god Chapery'),
(6, 'of the vicious elf Oughap'),
(7, 'of the dragon king Loril'),
(8, 'of the sealed Beliss'),
(9, 'of the abyssal Kimang'),
(10, 'of the mermaid Chreild');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- Indexes for dumped tables
--

--
-- Indexes for table `cell`
--
ALTER TABLE `cell`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_CB8787E253C55F64` (`map_id`);

--
-- Indexes for table `environnement`
--
ALTER TABLE `environnement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hero`
--
ALTER TABLE `hero`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_51CE6E86A76ED395` (`user_id`);

--
-- Indexes for table `hero_items`
--
ALTER TABLE `hero_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7E21E68A45B0BCD` (`hero_id`),
  ADD KEY `IDX_7E21E68A126F525E` (`item_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_type`
--
ALTER TABLE `item_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `left_handed_weapon`
--
ALTER TABLE `left_handed_weapon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map`
--
ALTER TABLE `map`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_93ADAABB45B0BCD` (`hero_id`);

--
-- Indexes for table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `right_handed_weapon`
--
ALTER TABLE `right_handed_weapon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suffixe_item`
--
ALTER TABLE `suffixe_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suffixe_map`
--
ALTER TABLE `suffixe_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cell`
--
ALTER TABLE `cell`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=711;
--
-- AUTO_INCREMENT for table `environnement`
--
ALTER TABLE `environnement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `hero`
--
ALTER TABLE `hero`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `hero_items`
--
ALTER TABLE `hero_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `item_type`
--
ALTER TABLE `item_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `left_handed_weapon`
--
ALTER TABLE `left_handed_weapon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `map`
--
ALTER TABLE `map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `right_handed_weapon`
--
ALTER TABLE `right_handed_weapon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `suffixe_item`
--
ALTER TABLE `suffixe_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `suffixe_map`
--
ALTER TABLE `suffixe_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cell`
--
ALTER TABLE `cell`
  ADD CONSTRAINT `FK_CB8787E253C55F64` FOREIGN KEY (`map_id`) REFERENCES `map` (`id`);

--
-- Constraints for table `hero`
--
ALTER TABLE `hero`
  ADD CONSTRAINT `FK_51CE6E86A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `hero_items`
--
ALTER TABLE `hero_items`
  ADD CONSTRAINT `FK_7E21E68A126F525E` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `FK_7E21E68A45B0BCD` FOREIGN KEY (`hero_id`) REFERENCES `hero` (`id`);

--
-- Constraints for table `map`
--
ALTER TABLE `map`
  ADD CONSTRAINT `FK_93ADAABB45B0BCD` FOREIGN KEY (`hero_id`) REFERENCES `hero` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
